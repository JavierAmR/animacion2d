﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    // Update is called once per frame
    void Update()
    {
        this.gameObject.transform.position = new Vector3 (target.position.x, target.position.y, this.gameObject.transform.position.z);
    }

}
