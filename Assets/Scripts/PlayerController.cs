﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public float jumpspeed;
    Vector2 movement;
    Rigidbody2D rb2d = null;
    public Animator animatorController;
    bool jump;
    bool isGrounded;
    bool isDead = false;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();

        

    }

    private void Update()
    {
        if (isDead == false)
        {

        
            if (Input.GetAxis("Horizontal") < 0)
            {
                gameObject.transform.localScale = new Vector3(-0.37291f, 0.37291f, 0.37291f);
                animatorController.SetInteger("Velocidad", (int)rb2d.velocity.x *-1);
            }
            else if (Input.GetAxis("Horizontal") > 0)
            {
                gameObject.transform.localScale = new Vector3(0.37291f, 0.37291f, 0.37291f);
                animatorController.SetInteger("Velocidad", (int)rb2d.velocity.x);
            }
            else
            {
                animatorController.SetInteger("Velocidad", 0);
            }
            jump = Input.GetKeyDown("space");



            if (jump && isGrounded)
            {
                rb2d.AddForce(Vector2.up * jumpspeed, ForceMode2D.Impulse);
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {       
        if (isDead == false)
        {
            rb2d.AddForce(Vector2.right*Input.GetAxis("Horizontal") * speed);
        }
        
        
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isGrounded = true;
        animatorController.SetBool("Saltando", false);
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        if (other.gameObject.tag == "Obstaculo")
        {
            animatorController.SetTrigger("Muerto");
            isDead = true;
        }
        if (other.gameObject.tag == "Trampolin")
        {
            rb2d.AddForce(Vector2.up * 13, ForceMode2D.Impulse);
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isGrounded = false;
        animatorController.SetBool("Saltando", true);
    }

    
}
